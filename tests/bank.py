import unittest
from src.bank import Account, ATM, Card


class BankTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.first = 'Mateusz'
        cls.last = 'Janikowski'
        cls.number = 5900932307508242342
        cls.balance = 10000

    def setUp(self):pass

    def test_check_pin(self):
        mastercard = Card(5623896578326, 2343)
        self.assertEqual(mastercard.check_pin(1111), False)
        self.assertEqual(mastercard.check_pin(2343), True)


    def test_account_balance(self):
        account = Account(
            self.first, self.last, self.number, self.balance)
        account.transfer(5000)
        self.assertEqual(account.balance, 15000)

        account2 = Account(self.first, self.last, self.number, 1000)

        with self.assertRaises(ValueError):
            account2.transfer(-5000)


class ATMTestCase(unittest.TestCase):
    """"""

    def test_withdraw(self):
        account = Account('Marcin', 'Olejniczak', 12345, 1000)
        card = Card(account, 1234)
        atm = ATM()
        atm.withdraw(card, 1234, 500)
        self.assertEqual(account.balance, 500)

